import React from 'react'
import PropTypes from 'prop-types'
import './Square.css'

export default class Square extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            currentColor: this.props.initialColor,
            // buttonText: 'Hide',
            visible: this.props.visible,
        }
    }

    setCurrentColor (currentColor) {
        this.setState({
            currentColor,
        })
    }

    // changeVisibility () {
    //     if(this.state.visible) {
    //         this.setState({
    //             buttonText: 'Show',
    //             visible: false
    //         })
    //     } else {
    //         this.setState({
    //             buttonText: 'Hide',
    //             visible: true
    //         })
    //     }
    // }
    toggleIsHidden () {
        this.setState((currentState) => {
            return {
                visible: !currentState.visible
            }
        })
    }

    render () {
        console.log(this.state)
        let textInput
        const size = `${this.props.size}px`

        const style = {
            width: size,
            height: size,
            backgroundColor: this.state.currentColor,
            display: this.state.visible ? 'block' : 'none',

        }
        return (
            <div className='Container'>
                <div style={style} className='Square'></div>

                <div className="SquareControls">
                        <input ref={(element) => { textInput = element; }}
                               type="text" placeholder='Color...'/>
                        <button className='ok'
                                onClick={() => {this.setCurrentColor(textInput.value)}}>Ok</button>
                        <button onClick={() => this.toggleIsHidden()}>{this.state.visible ? 'Hide' : 'Show'}</button>
                </div>
            </div>
        )
    }
}

Square.propTypes = {
    initialColor: PropTypes.string,
    size: PropTypes.number.isRequired,
    visible:PropTypes.bool,
}

Square.defaultProps = {
    initialColor: 'blue',
    visible: true,
}
