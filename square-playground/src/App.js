import React from 'react';
import logo from './logo.svg';
import './App.css';
import Square from './Square'

class App extends React.Component {
  render() {
    return (
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo"/>
          </header>
          <Square initialColor='red' size={100}/>
          {/*<SimpleSquare initialColor='green' size={100}/>*/}
          {/*<SimpleSquare initialColor='red' size={200}/>*/}
        </div>
    );
  }
}

export default App;
