import React from 'react';

export default class Counter extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            currentNumber: this.props.initialNumber,
        }

    }

    increment () {
        this.setState((prevState) => ({
            currentNumber: prevState.currentNumber + 1,
        }))
    }

    decrement () {
        this.setState((prevState) => ({
            currentNumber: prevState.currentNumber > this.props.initialNumber ? prevState.currentNumber - 1 : prevState.currentNumber,
        }))
        // this.setState((prevState) => ({
        //     currentNumber: (() => {
        //         if (prevState.currentNumber > this.props.initialNumber) {
        //             return prevState.currentNumber - 1
        //         } else {
        //             return prevState.currentNumber
        //         }
        //     })()
        // }))
    }

    render () {
        return (
            <div>
                <span>{this.state.currentNumber}</span>
                <button onClick={()=> this.increment()}>Increment</button>
                <button onClick={()=> this.decrement()}>Decrement</button>
            </div>
        )
    }
}
